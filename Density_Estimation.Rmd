---
title: ""
author: "Yandong Han"
date: "9/28/2021"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
library(mclust)
```

### A brief description of the data.

```{r}
# phoneme = read.csv("phoneme.csv", stringsAsFactors=TRUE)
phoneme = read.csv("~/UNI_HOME/769/lab10/phoneme.csv", stringsAsFactors=TRUE)
attach(phoneme)
 head(phoneme)
print(paste0("is missing value: ", sum(is.na(phoneme))))
plot(phoneme, phoneme$Class)

```


```{r}
with(phoneme, pairs(phoneme[,-6], col=c(2,4)[Class], pch=c(1,3)[Class]))
```


```{r}
if(sum(phoneme$Class=="Oral") >sum(phoneme$Class=="Nasal")){
  phoneme1<-rbind(phoneme[phoneme$Class=="Oral",],phoneme[phoneme$Class=="Nasal",])
}else{
   phoneme1<-rbind(phoneme[phoneme$Class=="Nasal",],phoneme[phoneme$Class=="Oral",])
}
with(phoneme1,pairs(phoneme1[,-6],col=c(2,4)[Class],pch=c(1,3)[Class]))
```






```{r}
hist(phoneme$V1, freq=FALSE, breaks=100)  
```

bandwidth=nrd0 looking like to underfitted,
other bandwidth value looking like overfitted
```{r}
par(mfrow=c(2,2))
for(bw in c("nrd0", "ucv", "bcv", "SJ")) {
   hist(phoneme$V1, freq=FALSE, breaks=40, 
        col="gray80", border="white",
        main=paste0("h = ", bw), xlab="phoneme$V1")
   lines(density(phoneme$V1, bw=bw), col=4, lwd=2)
}
```




```{r}
(r.q3 = densityMclust(phoneme$V1,G=1:20, modelNames=c("E","V")))
plot(r.q3, phoneme$V1, "density", breaks=100, lwd=2, col=2) 

```





```{r}
df <- subset(phoneme, select = -c(Class))
df$class=as.numeric(phoneme$Class)

```

```{r}

df.Nasal <- subset(phoneme, Class=="Nasal")
df.Nasal$Class=as.numeric(df.Nasal$Class)
(r.q4.Nasal = densityMclust(df.Nasal , G=1:9, modelNames="EEE"))
with(r.q4.Nasal, pairs(df.Nasal[,-6], col=c(2,4)[Class], pch=c(1,3)[Class]))

df.Oral <- subset(phoneme, Class=="Oral")
df.Oral$Class=as.numeric(df.Oral$Class)
(r.q4.Oral = densityMclust(df.Oral , G=1:9, modelNames="EEE"))    
with(r.q4.Oral, pairs(df.Oral[,-6], col=c(2,4)[Class], pch=c(1,3)[Class]))

```




```{r}
r.q5.Nasal = densityMclust(df.Nasal , G=1:9, modelNames="VVV")
with(r.q5.Nasal, pairs(df.Nasal[,-6], col=c(2,4)[Class], pch=c(1,3)[Class]))


r.q5.Oral = densityMclust(df.Oral , G=1:9, modelNames="VVV")
with(r.q5.Oral, pairs(df.Oral[,-6], col=c(2,4)[Class], pch=c(1,3)[Class]))
```


```{r}
df <- subset(phoneme, select = -c(Class))
df$class=as.numeric(phoneme$Class)

r.e = densityMclust(df , G=1:9, modelNames="EEE")
r.v = densityMclust(df , G=1:9, modelNames="VVV")


misclassification = function(ypred){
        confusion=table(df$class, ypred)
        rate <- 1 - (sum(diag(confusion))/sum(confusion))
        return(rate)
        # print(paste0("misclassification rate:", rate))
        
        
}
print(paste0("equal variance misclassification rate: ", misclassification(r.e$classification)))

print(paste0("equal variance misclassification rate: ", misclassification(r.v$classification)))


```




```{r}
r0 = kmeans(df, centers=2)

# with(df, plot(V1, V2, col=r0$cluster+1, main="K = 2 (Original Data)"))
# with(df, plot(V1, V3, col=r0$cluster+1, main="K = 2 (Original Data)"))



with(df,pairs(df[,-6],col=r0$cluster+1,pch=c(1,3)[Class]))
```


```{r}
df2 = as.data.frame(scale(df))   
head(df2)

ari = double(5)
for(k in 2:9) {
  r = kmeans(df2, centers=k)
  ari[k-1] = adjustedRandIndex(r0$cluster, r$cluster)
}
ari
```



## Mixture-based clustering


```{r}


ari = double(8)
for(k in 2:9) {
  r = Mclust(phoneme1[,-6], G=k, modelNames="VVV")
  ari[k-1] = adjustedRandIndex(phoneme1$Class, predict(r)$classification )
}
ari
```




## Hierarchical Clustering

```{r}
df2 = as.data.frame(scale(df))  
cex = 2
d = dist(df2)          # pairwise Euclidean distances

r.complete = hclust(d)   

# names(r)
plot(r.complete, cex.axis=cex, cex.lab=cex, cex.main=cex)       # 


```

```{r}
par(mfrow=c(2,2))
r.single = hclust(d, method="single")                # single linkage
# for(k in 2:5)                # single linkage
#   plot(phoneme, waiting, col=cutree(r.single,k)+1, main=paste0("K = ", k),
#        cex=cex, cex.axis=cex, cex.lab=cex, cex.main=cex)

plot(r.single, cex.axis=cex, cex.lab=cex, cex.main=cex) 
```





```{r}
ari = double(5)
for(k in 2:9) {
  
  ari[k-1] = adjustedRandIndex(cutree(r.complete,k) , cutree(r.single,k) )
}
ari
```




```{r}

heatmap(as.matrix(df2), scale="column", distfun=dist, hclustfun=hclust,
        margins=c(15,5))
```

```{r}
heatmap(as.matrix(df2), scale="column", distfun=dist, hclustfun=hclust,
        margins=c(15,5))
```
























































































